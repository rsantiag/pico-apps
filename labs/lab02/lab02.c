// #define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h"


/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    // Print a console message to inform user what's going on.
    printf("Hello World!\n");
    wallisProductSingle();
    wallisProductDouble();

    // Returning zero indicates everything went okay.
    return 0;
}

// Constant for PI
double PI = 3.14159265359;

/**
 * @brief wallisProductSingle
 *        Calculates Pi using single point precision
 *        Prints calculation for Pi and the Approximation Error
 * 
 * @return void
 */
void wallisProductSingle() {
    float pi = 1;
    for(int i = 1; i <= 100000; i++) {
        pi *= (4.0 * i * i) / ((4.0 * i * i) - 1);
    } 
    pi = 2 * pi;
    printf("PI (float) = %f\n", pi);
    float approxError = PI - pi;
    printf("Approximation Error = %f\n", approxError);
}

/**
 * @brief wallisProductDouble
 *        Calculates Pi using double point precision
 *    S    Prints calculation for Pi and the Approximation Error
 * 
 * @return void
 */
void wallisProductDouble() {
    double pi = 1;
    for(int i = 1; i <= 100000; i++) {
        pi *= (4.0 * i * i) / ((4.0 * i * i) - 1);
    } 
    pi = 2 * pi;
    printf("PI (double) = %f\n", pi);
    double approxError = PI - pi;
    printf("Approximation Error = %f\n", approxError);
}
