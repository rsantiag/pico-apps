#include <stdio.h>
#include <stdbool.h>
#include "pico/stdlib.h"
#include "pico/float.h"
#include "pico/double.h"
#include "pico/multicore.h"
#include "hardware/structs/xip_ctrl.h"

void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

// Function to get the enable status of the XIP cache
bool get_xip_cache_en() {
    uint64_t mask = 1;
    uint64_t result = xip_ctrl_hw->ctrl & mask;
    if(result > 0) {
        return true;
    }
    else {
        return false;
    }
}

// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en) {
    if(cache_en) {
        xip_ctrl_hw->flush = 1;
        xip_ctrl_hw->flush = 0;
        uint64_t mask = 1;
        uint64_t ctrl = xip_ctrl_hw->ctrl;
        uint64_t result = ctrl | mask;
        xip_ctrl_hw->ctrl = result;
        return true;
    }
    else {
        xip_ctrl_hw->flush = 1;
        xip_ctrl_hw->flush = 0;
        uint64_t mask = 0xFFFFFFFE;
        uint64_t ctrl = xip_ctrl_hw->ctrl;
        uint64_t result = ctrl & mask;
        xip_ctrl_hw->ctrl = result;
        return true;
    }
}

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

    int start, end, diff;

    const int ITER_MAX = 100000;

    stdio_init_all();

    // Launch core1
    multicore_launch_core1(core1_entry);

    set_xip_cache_en(true);                                 // Enable cache
    printf("(Cache enabled)\n");                               
    printf("Sequential Run:\n");                            // Code for sequential run
    start = time_us_64();                                   // Get time at the start of the sequential run of code
    wallisProductSingle(ITER_MAX);                          // Run wallisProductSingle() on core0
    wallisProductDouble(ITER_MAX);                          // Run wallisProductDouble() on core0
    end = time_us_64();                                     // Get time at the end of the sequential run of code    
    diff = end - start;                                     // Calculate the difference between start and end
    printf("Total time taken = %d microseconds\n", diff);   // Print the result

    set_xip_cache_en(false);                                // Disable cache
    printf("\n(Cache disabled)\n");                               
    printf("Sequential Run:\n");                            // Code for sequential run
    start = time_us_64();                                   // Get time at the start of the sequential run of code
    wallisProductSingle(ITER_MAX);                          // Run wallisProductSingle() on core0
    wallisProductDouble(ITER_MAX);                          // Run wallisProductDouble() on core0
    end = time_us_64();                                     // Get time at the end of the sequential run of code    
    diff = end - start;                                     // Calculate the difference between start and end
    printf("Total time taken = %d microseconds\n", diff);   // Print the result

    set_xip_cache_en(true);                                                     // Enable cache
    printf("\n(Cache enabled)\n");   
    printf("Parallel Run:\n");                                                // Code for parallel run
    start = time_us_64();                                                       // Get time at the start of the parallel run of code
    multicore_fifo_push_blocking((uintptr_t) &wallisProductSingle);             // Run wallisProductSingle() on core1
    multicore_fifo_push_blocking(ITER_MAX);                                     // Pass the parameter needed for wallisProductSingle()
    wallisProductDouble(ITER_MAX);                                              // Run wallisProductDouble on core0
    int result = multicore_fifo_pop_blocking();                                 // Pop the return value from the stack
    end = time_us_64();                                                         // Get time at the end of the parallel run of code
    diff = end - start;                                                         // Calculate the difference between start and end
    printf("Total time taken = %d microseconds\n", diff);                       // Print the result

    set_xip_cache_en(false);                                                    // Disable cache
    printf("\n(Cache disabled)\n");   
    printf("Parallel Run:\n");                                                // Code for parallel run
    start = time_us_64();                                                       // Get time at the start of the parallel run of code
    multicore_fifo_push_blocking((uintptr_t) &wallisProductSingle);             // Run wallisProductSingle() on core1
    multicore_fifo_push_blocking(ITER_MAX);                                     // Pass the parameter needed for wallisProductSingle()
    wallisProductDouble(ITER_MAX);                                              // Run wallisProductDouble on core0
    result = multicore_fifo_pop_blocking();                                 // Pop the return value from the stack
    end = time_us_64();                                                         // Get time at the end of the parallel run of code
    diff = end - start;                                                         // Calculate the difference between start and end
    printf("Total time taken = %d microseconds\n", diff);                       // Print the result
    
    // Returning zero indicates everything went okay.
    return 0;
}

// Constant for PI
double PI = 3.14159265359;

/**
 * @brief wallisProductSingle
 *        Calculates Pi using single point precision
 *        Prints calculation for Pi and the Approximation Error
 *        Prints time taken in microseconds
 *        Takes in the number of iterations to perform as a parameter
 * 
 * @return void
 */
 int wallisProductSingle(int iterations) {
    int to, from, diff;
    from = time_us_64();
    float pi = 1;
    for(int i = 1; i <= iterations; i++) {
        pi *= (4.0 * i * i) / ((4.0 * i * i) - 1);
    } 
    pi = 2 * pi;
    printf("PI (float) = %f\n", pi);
    float approxError = PI - pi;
    printf("Approximation Error = %f\n", approxError);
    to = time_us_64();
    diff = to - from;
    printf("wallisProductSingle(): Time taken = %d microseconds\n", diff);
    return 0;
}

/**
 * @brief wallisProductDouble
 *        Calculates Pi using double point precision
 *        Prints calculation for Pi and the Approximation Error
 *        Prints time taken in microseconds
 *        Takes in the number of iterations to perform as a parameter
 * 
 * @return void
 */
int wallisProductDouble(int iterations) {
    int to, from, diff;
    from = time_us_64();
    double pi = 1;
    for(int i = 1; i <= iterations; i++) {
        pi *= (4.0 * i * i) / ((4.0 * i * i) - 1);
    } 
    pi = 2 * pi;
    printf("PI (double) = %f\n", pi);
    double approxError = PI - pi;
    printf("Approximation Error = %f\n", approxError);
    to = time_us_64();
    diff = to - from;
    printf("wallisProductDouble(): Time taken = %d microseconds\n", diff);
    return 0;
}
